#include <avr/io.h>
#include <util/delay.h>
#include <avr/interrupt.h>
#include <stdio.h>
#include <string.h>

#define BAUD1 1200
#define BAUD2 2400
#define BAUD3 4800
#define BAUD4 9600
#define BAUD5 19200
#define BAUD6 38400
#define BAUD7 57600
#define BAUD8 115200
#define BAUD9 230400
#define BAUD10 250000


#define UBRRVAL F_CPU/8/BAUD-1 /* calculates UBRRVAL for BAUD baudrate, BAUD must be defined */
#define TX_BUFFER 128

#define buffer_size  64
char rxbuffer[buffer_size];
char txbuffer[]="00010110";
volatile int commandflag  = 0;
int readflag = 0;
int readpos =0;
int writepos=0;
volatile int checkcalc=0;



static int uputc(char,FILE*);
void validatecommand();
void commands();



static int uputc(char c,FILE *stream)
/* uart writer function for libc stdio functions */
{
	if (c == '\n')
		uputc('\r',stream);
	loop_until_bit_is_set(UCSR0A, UDRE0);	/* wait until we can send a new byte */
	UDR0 = (uint8_t) c;
	return 0;
}

void validatecommand()     							// function for checking if command has been sent and if it is valid by looking at the checksum
{
		if (commandflag==1)  						//if called when commandflag set at 1, validates received command and goes to command function
		{
			char checkchar = (checkcalc)%10+48;
			if (rxbuffer[readpos-2] == checkchar)
			{
				printf("\ncheck is valid\n");
				commands();
			}
			else
			{
				printf("%c\n",rxbuffer[readpos-2]);
				printf("%c\n",checkchar);
				printf("check is not valid\n");
			}

			PORTB ^=(1<<PORTB5);  					// visual sign of validatecommand being called
			commandflag=0;							//setting important command values at 0 so they can be changed again
			checkcalc=0;
			readpos=0;
		}
}

void commands()										//state machine type command function, depending on initial command byte,
{													//, executes different tasks then responds

	char commandtype = rxbuffer[0];
	int datasize = rxbuffer[1]-48;

	switch (commandtype)
	{
	case 'T':
		printf("\n !T");
		printf("%d",datasize);
		for (size_t i = 2; i < datasize+2; i++)
		{
			printf("%c",rxbuffer[i]);
			}
		UDR0 = '\n';
	break;

	case 'B':
		printf("\ncommand B\n");
	break;

	case 'C':

		printf("\ncommand C\n");
	break;

	default:
	printf("\nno command type\n");
	break;
	}
}


/* define writer function as a stdio stream */
static FILE mystdout = FDEV_SETUP_STREAM(uputc, NULL,_FDEV_SETUP_WRITE);


//uart receive isr
ISR(USART_RX_vect)
{

	uint8_t c = UDR0;   // Fetch  the  received  byte  value  into  the  variable c
	
	switch (readflag)    					//condition set after '!' start bit to start registering command and data to be send
	{
		case 1:
				rxbuffer[readpos]=c;
				readpos++;
		break;

	default :
		break;
	}
	loop_until_bit_is_set(UCSR0A, UDRE0);		/* wait until we can send a new byte */
	UDR0 = (uint8_t) c; 						 // Echo  back  the  received  byte  back to the  computer

	if (c=='!')  								// sets readflag at 1, making bytes being received in the next cycle to be stored in rxbuffer
		{
			readflag=1;
		}

	if (c=='\r')								//stop bit, sets command flag at 1, sets readflag at 0, calling validatecommand in main and stops storing incoming bytes to rxbuffer
	{
		for (size_t i = 0; i < readpos-2; i++)
		{
			checkcalc+=rxbuffer[i];  			//sums all bytes on buffer from command byte to last data byte before stop byte '\r'
		}
		commandflag=1;
		readflag=0;
	}
}


int main(void)
{
	/* tell stdio to use our stream as default */
	stdout=&mystdout;
	txbuffer[8]='\0';

	/* pin config */
	DDRB = (1 << DDB5);
	
	/* uart config */
	UCSR0A = (1 << U2X0); /* this sets U2X0 to 1 */   // doubles the rate of transmission 
	UCSR0B = (1 << RXEN0) | (1 << TXEN0) | (1 << RXCIE0); // Enable receiver, Enable Transmitter, Enable Receiver Complete Interrupt- call an interrup when done receiving
	UCSR0C = (3 << UCSZ00); /* this sets UCSZ00 to 3 */  //sets number of data bits( character size) to be used 3-> 011-> 8bit data

	/* baudrate setings (variable set by macros) */
	UBRR0H = (UBRRVAL) >> 8;   //Load  upper 8-bits of the  baud  rate  value  into  the  high  byteof the  UBRR  register
	UBRR0L = UBRRVAL;			// Load  lower 8-bits of the  baud  rate  value  into  the  low  byte of theUBRR  register

	/* ADC cfg */
	sei();							  /* enable external interrupts */
	puts("Hi");
	printf("%d\n",checkcalc);
	
	for (;;) 
	{
		validatecommand();     		// validatecommand always being called
	}
	

}

