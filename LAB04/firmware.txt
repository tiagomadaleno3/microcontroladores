
/home/tiago/Documents/PlatformIO/Projects/ArduinoNano328_LAB01/.pio/build/nanoatmega328/firmware.elf:     file format elf32-avr


Disassembly of section .text:

00000000 <__vectors>:
   0:	0c 94 34 00 	jmp	0x68	; 0x68 <__ctors_end>
   4:	0c 94 3e 00 	jmp	0x7c	; 0x7c <__bad_interrupt>
   8:	0c 94 3e 00 	jmp	0x7c	; 0x7c <__bad_interrupt>
   c:	0c 94 3e 00 	jmp	0x7c	; 0x7c <__bad_interrupt>
  10:	0c 94 3e 00 	jmp	0x7c	; 0x7c <__bad_interrupt>
  14:	0c 94 3e 00 	jmp	0x7c	; 0x7c <__bad_interrupt>
  18:	0c 94 3e 00 	jmp	0x7c	; 0x7c <__bad_interrupt>
  1c:	0c 94 3e 00 	jmp	0x7c	; 0x7c <__bad_interrupt>
  20:	0c 94 3e 00 	jmp	0x7c	; 0x7c <__bad_interrupt>
  24:	0c 94 3e 00 	jmp	0x7c	; 0x7c <__bad_interrupt>
  28:	0c 94 3e 00 	jmp	0x7c	; 0x7c <__bad_interrupt>
  2c:	0c 94 3e 00 	jmp	0x7c	; 0x7c <__bad_interrupt>
  30:	0c 94 3e 00 	jmp	0x7c	; 0x7c <__bad_interrupt>
  34:	0c 94 3e 00 	jmp	0x7c	; 0x7c <__bad_interrupt>
  38:	0c 94 3e 00 	jmp	0x7c	; 0x7c <__bad_interrupt>
  3c:	0c 94 3e 00 	jmp	0x7c	; 0x7c <__bad_interrupt>
  40:	0c 94 3e 00 	jmp	0x7c	; 0x7c <__bad_interrupt>
  44:	0c 94 3e 00 	jmp	0x7c	; 0x7c <__bad_interrupt>
  48:	0c 94 3e 00 	jmp	0x7c	; 0x7c <__bad_interrupt>
  4c:	0c 94 3e 00 	jmp	0x7c	; 0x7c <__bad_interrupt>
  50:	0c 94 3e 00 	jmp	0x7c	; 0x7c <__bad_interrupt>
  54:	0c 94 3e 00 	jmp	0x7c	; 0x7c <__bad_interrupt>
  58:	0c 94 3e 00 	jmp	0x7c	; 0x7c <__bad_interrupt>
  5c:	0c 94 3e 00 	jmp	0x7c	; 0x7c <__bad_interrupt>
  60:	0c 94 3e 00 	jmp	0x7c	; 0x7c <__bad_interrupt>
  64:	0c 94 3e 00 	jmp	0x7c	; 0x7c <__bad_interrupt>

00000068 <__ctors_end>:
  68:	11 24       	eor	r1, r1
  6a:	1f be       	out	0x3f, r1	; 63
  6c:	cf ef       	ldi	r28, 0xFF	; 255
  6e:	d8 e0       	ldi	r29, 0x08	; 8
  70:	de bf       	out	0x3e, r29	; 62
  72:	cd bf       	out	0x3d, r28	; 61
  74:	0e 94 40 00 	call	0x80	; 0x80 <main>
  78:	0c 94 51 00 	jmp	0xa2	; 0xa2 <_exit>

0000007c <__bad_interrupt>:
  7c:	0c 94 00 00 	jmp	0	; 0x0 <__vectors>

00000080 <main>:
#include "read.h"

int main(void)
{
	/* I/O cfg */
	DDRB = (1 << DDB5); // DDRB = 0b 0010 0000. sets PORTB5 as an output. Corresponds to PIN 5 which is the built in LED D13 on arduino
  80:	80 e2       	ldi	r24, 0x20	; 32
  82:	84 b9       	out	0x04, r24	; 4
	PORTB = (1<<PORTB4); // PORTB = 0b 0001 0000. PORTB4 is an input. Pull-up resistor activated in PIN4. Prevents the pin from floating
  84:	80 e1       	ldi	r24, 0x10	; 16
  86:	85 b9       	out	0x05, r24	; 5
	}*/

// alinea 7 - b
	
	do{
		PORTB &= ~(1<<PORTB5); // PORTB = 0001 0000 & 1101 1111 = 0001 0000 which keeps PORTB4 pull up activated,
  88:	85 b1       	in	r24, 0x05	; 5
  8a:	8f 7d       	andi	r24, 0xDF	; 223
  8c:	85 b9       	out	0x05, r24	; 5
									// next cyle from else PORTB = 0011 0000. then 0011 0000 & 1101 1111 = 0001 0000
									// on PIN 4 (D12 on arduino) and leads to PORTB5 led turn off (bit is 0)

	}
	while (bit_is_set(PINB,PINB4)); //returns 1 if the bit in PINB4 is set (interruptor open - reads bit high) else 
  8e:	1c 99       	sbic	0x03, 4	; 3
  90:	fb cf       	rjmp	.-10     	; 0x88 <main+0x8>
									// returns 0 if the bit in PIN4 is clear (interruptor closed - reads bit low )
			

	do{
		PORTB |= (1<<PORTB5); // PORTB = 0001 0000 | 0010 0000 = 0011 0000 which keeps PORTB4 pull up activated, 
  92:	85 b1       	in	r24, 0x05	; 5
  94:	80 62       	ori	r24, 0x20	; 32
  96:	85 b9       	out	0x05, r24	; 5
									// next cycle if bit is clear PORTB = 0011 0000. then: 0011 0000 | 0010 0000 = 0011 0000
								  //on PIN4 (D12 on arduino) and leads to PORTB5 led turn on (bit is 1)s

	}
	while (bit_is_clear(PINB,PINB4)); //returns 1 if the bit in PINB4 is clear (interruptor closed - reads bit low) else 
  98:	1c 9b       	sbis	0x03, 4	; 3
  9a:	fb cf       	rjmp	.-10     	; 0x92 <main+0x12>
									// returns 0 if the bit in PIN4 is set (interruptor opem - reads bit high )
		
	

	//main_loop(); 
  9c:	90 e0       	ldi	r25, 0x00	; 0
  9e:	80 e0       	ldi	r24, 0x00	; 0
  a0:	08 95       	ret

000000a2 <_exit>:
  a2:	f8 94       	cli

000000a4 <__stop_program>:
  a4:	ff cf       	rjmp	.-2      	; 0xa4 <__stop_program>
