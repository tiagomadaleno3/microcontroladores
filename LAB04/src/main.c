#include <avr/io.h>
#include <util/delay.h>
#include <avr/interrupt.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>



#define UBRRVAL F_CPU/8/BAUD-1 /* calculates UBRRVAL for BAUD baudrate, BAUD must be defined */


#define buffer_size  128
char rxbuffer[buffer_size];
volatile int commandflag  = 0;
int readflag = 0;
int readpos =0;
int dcflag =0;
double dutycicle = 50.0;
int tensao = 0;
int NEWTOP=65020;
int val;
int ADCval;


static int uputc(char,FILE*);
void validatecommand();
void commands();
void timer1config();
void ADCconfig();
void dutyciclechanger();

static int uputc(char c,FILE *stream)
/* uart writer function for libc stdio functions */
{
	if (c == '\n')
		uputc('\r',stream);
	loop_until_bit_is_set(UCSR0A, UDRE0);	/* wait until we can send a new byte */
	UDR0 = (uint8_t) c;
	return 0;
}

void validatecommand(){   							// function for checking if command has been sent
		if (commandflag==1)  						//if called when commandflag set at 1, validates received command and goes to commands() function
		{
			commands();
			commandflag=0;							//setting important command values at 0 so they can be changed again
			readpos=0;
		}
}

void commands()										//state machine type command function, depending on initial command byte,
{													//, executes different tasks then responds

	char commandtype = rxbuffer[0];
	char *numptr = &rxbuffer[1];
	val= atoi(numptr);

	switch (commandtype)
	{
	case 'D':									  //attributes a dutycicle value as long as its less than 100%

		if (val >100)
		{
			printf("dutycicle > 100");
		}
		else
		{
			dutycicle=val;
			printf("\n dutycicle changed to %d\n", val);
		}break;

	case 'F':													//changes the frequency 
		printf("\nchanging frequency to %uHZ\n",val);
		NEWTOP=-1+16000000.0/(val);
		printf("\n %u\n",ICR1);
	break;

	case 'C':													// enables output compare interrupt which is set up to work with a fixed dutycicle value
		printf("\nnormal pwm\n");
		dcflag=0;
	break;

	case 'O':													//overwrites and enables overflow interrupt which changes the dutycicle automatically, generating
		printf("\nTrue wave function\n");						//a visible wave in the LED
		dcflag=1;
	break;

    case 'A':
        ADCSRA |=(1<<ADSC); 
        printf("\nConversion command\n");
    break;

	  case 'T':   //automatic trigger
	  dcflag=0;
        ADCSRA = (1<<ADATE)|(1<<ADPS2)|(1<<ADPS1)|(1<<ADPS0) ; 
		ADCSRB |= (1<<ADTS1) | (1<<ADTS2);
		TIMSK1 |=(1<<TOIE1);
    break;


	default:
	printf("\nno command type\n");
	break;
	}
}

void dutyciclechanger()
{
		dutycicle +=100.0/1024.0;
			if (dutycicle >100.0)
			{
			dutycicle=0.0;
			}
}

void ADCconfig()
{

    ADMUX = (1<<REFS0)|(1<<MUX0);  									//Vref= AVcc and use ADC1 pin
    ADCSRA= (1<<ADEN)|(1<<ADPS2)|(1<<ADPS1)|(1<<ADPS0)|(1<<ADIE);
    DIDR0 = (1<<ADC1D);

}


void timer1pwmconfig()																//exercice 2
{
	
	TCCR1A = (1<<COM1A1)|(1<<WGM11);				//timer set up in mode 14 with TOP and Output values given by global variables
	ICR1 = NEWTOP;                       						
	OCR1A = (dutycicle/100.0)*NEWTOP;
	TIMSK1 = (1<<OCIE1A);							//only output compare interrup enabled, making it work with a fixed dutycicle
	TCCR1B = (1<<WGM13)|(1<<WGM12);
	TCCR1B |= (1<<CS10);
}



ISR(TIMER1_COMPA_vect)								//Interrupt that automatically changes dutycicle and updates
{ 
	ICR1 = NEWTOP;
	OCR1A = (dutycicle/100.0)*ICR1;
}

ISR(TIMER1_OVF_vect) {								//Interruptor for automatic trigger
	
	ICR1 = NEWTOP;
	OCR1A = (dutycicle/100.0)*ICR1;
	puts("hello");
}

ISR(ADC_vect)
{
    ADCval = ADC;

	tensao = 5*OCR1A/ICR1;
	printf("\n tensao (V) = %d\n", tensao);
	//printf("\n tensao = %d\n", ICR1);
	printf("\n ADC = %d\n", ADCval);
}


static FILE mystdout = FDEV_SETUP_STREAM(uputc, NULL,_FDEV_SETUP_WRITE);


//uart receive isr
ISR(USART_RX_vect)
{

	uint8_t c = UDR0;
	
	switch (readflag)    					//condition set after '!' start bit to start registering command and data to be send
	{
		case 1:
			rxbuffer[readpos]=c;
			readpos++;
		break;

	default :
		break;
	}

	
	if (c=='!')  								// sets readflag at 1, making bytes being received in the next cycle to be stored in rxbuffer
		{
			readflag=1;
		}

	if (c=='\r')								//stop bit, sets command flag at 1, sets readflag at 0, calling validatecommand in main and stops storing incoming bytes to rxbuffer
	{
		commandflag=1;
		readflag=0;
	}

	loop_until_bit_is_set(UCSR0A, UDRE0);		/* wait until we can send a new byte */
	UDR0 = (uint8_t) c;

}


int main(void)
{
	/* tell stdio to use our stream as default */
	stdout=&mystdout;

	/* pin config */
	DDRB = (0<<PORTB5)|(1<<PORTB1);
	


	/* uart config */
	UCSR0A = (1 << U2X0); /* this sets U2X0 to 1 */   // doubles the rate of transmission 
	UCSR0B = (1 << RXEN0) | (1 << TXEN0) | (1 << RXCIE0); // Enable receiver, Enable Transmitter, Enable Receiver Complete Interrupt- call an interrup when done receiving
	UCSR0C = (3 << UCSZ00); /* this sets UCSZ00 to 3 */  //sets number of data bits( character size) to be used 3-> 011-> 8bit data

	/* baudrate setings (variable set by macros) */
	UBRR0H = (UBRRVAL) >> 8;
	UBRR0L = UBRRVAL;

	/* ADC cfg */
	sei();							  /* enable external interrupts */
	printf("ago\n");

	timer1pwmconfig();
	ADCconfig();
	for (;;) 
	{
		validatecommand();     		// validatecommand always being called
		if (dcflag==1){
			dutyciclechanger();
			_delay_ms(100);
			ADCSRA |=(1<<ADSC);
		}

	}
}
