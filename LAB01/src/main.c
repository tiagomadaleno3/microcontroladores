#include <avr/io.h>
#include <util/delay.h>
#include "read.h"
#include <avr/interrupt.h>

#define F_CPU 16000000UL // 16 MHZ clock

#define LED_ON	PORTB |= (1<<PORTB5)
#define LED_OFF	PORTB &= ~(1<<PORTB5)
#define LED_TOGGLE	PORTB ^= (1 << PORTB5) //PINB |= (1<<PINB5) It works in newer avr releases https://www.avrfreaks.net/forum/exclusive-or-output-port
#define DEBOUNCE_TIME 25
#define LOCK_INPUT_TIME 300

void IO_config(){
	/* I/O cfg */
	DDRB = (1 << DDB5); // DDRB = 0b 0010 0000. sets PORTB5 as an output. Corresponds to PIN 5 which is the built in LED D13 on arduino
	DDRB &= ~(1<< DDB4); //explicitly sets pin 4 (arduino pin 12) as an input
	PORTB = (1<<PORTB4); // PORTB = 0b 0001 0000. PORTB4 is an input. Pull-up resistor activated in PIN4. Prevents the pin from floating
}


unsigned char button_state (){
	if (bit_is_clear(PINB,PINB4)){
		_delay_ms(DEBOUNCE_TIME);
		if (bit_is_clear(PINB, PINB4)){
			return 1;
			}
	}
	return 0;
}

int main(void)
{
	
	/* I/O cfg */
	IO_config();

	//teste. Toggle pin
	/*
	DDRB = (1 << DDB5);
	while (1){
		PINB |= (1 << PINB5);
		_delay_ms(500);
	}
	*/
	
	
	// alinea 1
	
	/*for (;;) {
		if(bit_is_clear(PINB,PINB4)){ //returns 1 if the bit in PINB4 is set (interruptor open - reads bit high) else 
									// returns 0 if the bit in PIN4 is clear (interruptor closed - reads bit low )
			LED_OFF; // PORTB = 0001 0000 (line9: PORTB) & 1101 1111 = 0001 0000 which keeps PORTB4 pull up activated,
									// on PIN 4 (D12 on arduino) and leads to PORTB5 led turn off (bit is 0)
		
		} else{ 
			LED_ON; // PORTB = 0001 0000 (line 9: PORTB) | 0010 0000 = 0011 0000 which keeps PORTB4 pull up activated, 
								  //on PIN4 (D12 on arduino) and leads to PORTB5 led turn on (bit is 1)
			}	
	}*/

	// alinea 4 - 1
	
	/*for (;;) {
		if(bit_is_set(PINB,PINB4)){ //returns 1 if the bit in PINB4 is set (interruptor open - reads bit high) else 
									// returns 0 if the bit in PIN4 is clear (interruptor closed - reads bit low )
			LED_OFF; // PORTB = 0001 0000 & 1101 1111 = 0001 0000 which keeps PORTB4 pull up activated,
									// next cyle from else PORTB = 0011 0000. then 0011 0000 & 1101 1111 = 0001 0000
									// on PIN 4 (D12 on arduino) and leads to PORTB5 led turn off (bit is 0)
		
		} else{ 
			LED_ON; // PORTB = 0001 0000 | 0010 0000 = 0011 0000 which keeps PORTB4 pull up activated, 
									// next cycle if bit is clear PORTB = 0011 0000. then: 0011 0000 | 0010 0000 = 0011 0000
								  //on PIN4 (D12 on arduino) and leads to PORTB5 led turn on (bit is 1)s
			}
	}*/

	// alinea 4 - 2
	
	/*for (;;) {
		if(bit_is_set(PINB,PINB4)){ //returns 1 if the bit in PINB4 is set (interruptor open - reads bit high) else 
									// returns 0 if the bit in PIN4 is clear (interruptor closed - reads bit low )
			LED_OFF; // PORTB = 0001 0000 & 1101 1111 = 0001 0000 which keeps PORTB4 pull up activated,
									// next cyle from else PORTB = 0011 0000. then 0011 0000 & 1101 1111 = 0001 0000
									// on PIN 4 (D12 on arduino) and leads to PORTB5 led turn off (bit is 0)
		
		} else{ 
			LED_TOGGLE; // PORTB = 0001 0000 ^ 0010 0000 = 0011 0000 which keeps PORTB4 pull up activated, 
									// next cycle if bit is clear PORTB = 0011 0000. then: 0011 0000 ^ 0010 0000 = 0001 0000
								  //on PIN4 (D12 on arduino) and leads to PORTB5 led turn on and off (bit is 1)
			_delay_ms(500);			// defines the delay time
			}	
	}*/

	//alinea 7
	while(1){
		if(button_state()){
			LED_TOGGLE;
			_delay_ms(LOCK_INPUT_TIME);
		}
	}		
	//main_loop(); 
}