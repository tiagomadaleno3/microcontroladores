#include <avr/io.h>
#include <util/delay.h>
#include <avr/interrupt.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>


#define UBRRVAL F_CPU/8/BAUD-1 /* calculates UBRRVAL (=16) for BAUD baudrate, BAUD must be defined */

#define LED_TOGGLE	PORTB ^= (1<<PORTB5)



#define buffersize  8			/* setting universal variables*/
char rx_buffer[buffersize];
volatile int commandflag  = 0;
int readflag = 0; int writeflag = 0;
size_t readpos = 0;
volatile int checksum=0;
float duty_cycle = 0.0;



//static int uputc(char,FILE*);
//uint8_t ugetc(FILE*);

static int uputc(char c,FILE *stream) { /* uart writer function for libc stdio functions */
	if (c == '\n')
		uputc('\r',stream);
	loop_until_bit_is_set(UCSR0A, UDRE0);	/* wait until we can send a new byte */
	UDR0 = (uint8_t) c;
	return 0;
}


/* define writer function as a stdio stream */
static FILE mystdout = FDEV_SETUP_STREAM(uputc, NULL,_FDEV_SETUP_WRITE);
//static FILE mystdin = FDEV_SETUP_STREAM(NULL, ugetc, _FDEV_SETUP_READ);


/*void read_serial(char c){
	rx_buffer[readpos]=c;
	readpos++;
	if(readpos>=buffersize){ // preventing array overflow
		readpos=0;
	}
}*/

/*void protocol()
{

	
	if (commandflag==1) {

		char commandtype = rx_buffer[0];
		char *numptr = &rx_buffer[1];
		int val= atoi(numptr);

		switch (commandtype)
		{
		case 'D':
			printf("\nchanging duty cycle to %d\n",val);
			duty_cycle=val*10;							//changes TOP to half the value, making the clock cylce 2x faster
		break;

		case 'F':
			printf("\nchanging frequency\n");
		break;

		default:
		printf("\nno command type\n");
		break;
	}

		memset(rx_buffer, 0, sizeof(rx_buffer));
		

		commandflag=0;
		readpos=0;
		readflag=0;


	}
	_delay_ms(100);
	duty_cycle += 1;
	if(duty_cycle>100) {
		duty_cycle = 0;
	}

}*/
	
/*
ISR(USART_RX_vect){	//Interruptor service routine. When char is pressed this interruptor is lauched

	uint8_t c = UDR0;
	loop_until_bit_is_set(UCSR0A, UDRE0);	// wait until we can send a new byte 
	UDR0 = (uint8_t) c;


	if (readflag==1){		//building the array of chars
		read_serial(c);
	}

	if (c=='!'){			//when ! is pressed readflag=1 and the next char are going to the rx_buffer
		readflag=1;
	}

	if (c=='\r'){			//When enter is pressed  commandflag for protocol is 1t
		for (size_t i = 0; i < readpos-2; i++)
		{
			checksum+=rx_buffer[i];	//the ASCII values of the rx_buff before \r and verification integer are summed
		}
		commandflag=1;
	}
}*/

//alinea 1 

/*void timer_frequency(uint8_t frequency) {

	TCCR1A = 0; // set entire TCCR1A register to 0
	TCCR1B = 0; // same for TCCR1B
	TCNT1  = 0; // initialize counter value to 0

	//alinea 1 registers setup
	// turn on CTC mode
	TCCR1B |= (1 << WGM12);
	// Set CS12, CS11 and CS10 bits for 256 prescaler
	TCCR1B |= (1 << CS12) | (0 << CS11) | (0 << CS10);
	// enable timer compare interrupt
	TIMSK1 |= (1 << OCIE1A);

	int prescaler = 256;

	// set compare match register for increments
	OCR1A = F_CPU / (frequency* prescaler ) - 1; // 0xF423 for 1 Hz (must be <65536)

}*/

void pwm_generator() {
	
	TCCR1A = 0; // set entire TCCR1A register to 0
	TCCR1B = 0; // same for TCCR1B
	TCNT1  = 0; // initialize counter value to 0v
	//alinea 2 registers setup
	// enable timer compare interrupt
	TIMSK1 |= (1 << OCIE1B);

	//turn on fast PWM mode

	TCCR1A |= (1 << COM1A1)| (1 << COM1B1) | (1 << WGM10) | (1 << WGM11);
	TCCR1B |= (1 << WGM12) | (1 << WGM13);
	// Set for no prescaler
	TCCR1B |=  (1 << CS12) | (0 << CS11) | (1 << CS10);
}

ISR(TIMER1_COMPB_vect){
	OCR1A = F_CPU / (100000 * 1024 ) - 1; // 100 is frequency
	OCR1B = (duty_cycle/100)*OCR1A;
	
}

/*ISR(TIMER1_COMPA_vect){
	//alinea 1
	//OCR1A = 0x7A11; //31249;
	//LED_TOGGLE;
}*/


int main(void) {
	/* tell stdio to use our stream as default */
	stdout=&mystdout;

	//pin config for alinea 1
	//DDRB = (1 << DDB5);

	/* pin config for pwm */
	DDRB = (0 << PORTB5) | (1 << PORTB2);

	/* uart config */
	UCSR0A = (1 << U2X0); /* this sets U2X0 to 1 */
	UCSR0B = (1 << RXEN0) | (1 << TXEN0) | (1 << RXCIE0);
	UCSR0C = (3 << UCSZ00); /* this sets UCSZ00 to 3 */

	/* baudrate setings (variable set by macros) */
	UBRR0H = (UBRRVAL) >> 8;
	UBRR0L = UBRRVAL;


	//timer interrupts 
	cli(); // stop interrupts
	
	//timer_frequency(1); //0xF423 62499 for 1 Hz alinea 1
	
	//alinea 2
	pwm_generator();


	/* ADC cfg */
	sei();							  /* enable interrupts */

	//char input;
	//protocol_transmiter();
	//alinea 1
	//for(;;){};
	
	
	// alinea 2
	for(;;){
	
		//duty_cycle = 100;

		//_delay_ms(100);
		duty_cycle += 10;
		if(duty_cycle>100) {
			duty_cycle = 0;
		}

	}
}
