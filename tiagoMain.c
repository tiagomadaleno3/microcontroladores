#include <avr/io.h>
#include <util/delay.h>
#include <avr/interrupt.h>
#include <stdio.h>
#include <string.h>


#define UBRRVAL F_CPU/8/BAUD-1 /* calculates UBRRVAL (=16) for BAUD baudrate, BAUD must be defined */

#define LED_TOGGLE	PORTB ^= (1<<PORTB5)

#define buffersize  8			/* setting universal variables*/
char rx_buffer[buffersize];
char tx_buffer[buffersize];
volatile int commandflag  = 0;
int readflag = 0; int writeflag = 0;
size_t readpos = 0;
volatile int checksum=0;


//static int uputc(char,FILE*);
//uint8_t ugetc(FILE*);

static int uputc(char c,FILE *stream) { /* uart writer function for libc stdio functions */
	if (c == '\n')
		uputc('\r',stream);
	loop_until_bit_is_set(UCSR0A, UDRE0);	/* wait until we can send a new byte */
	UDR0 = (uint8_t) c;
	return 0;
}

/* define writer function as a stdio stream */
static FILE mystdout = FDEV_SETUP_STREAM(uputc, NULL,_FDEV_SETUP_WRITE);
//static FILE mystdin = FDEV_SETUP_STREAM(NULL, ugetc, _FDEV_SETUP_READ);

void read_serial(char c){
	rx_buffer[readpos]=c;
	readpos++;
	if(readpos>=buffersize){ /*preventing array overflow*/
		readpos=0;
	}
}

void protocol_receiver()
{
	if (commandflag==1) {

		char checkchar = (checksum)%10+48; /*calculates the modulus of checksum */

		printf("\nASCII sum of characters = %d\n",checksum);
		printf("resto do modulo 10 = %c\n",checkchar);
		printf("ultimo caracter inserido = %c\n",rx_buffer[readpos-3]);

		if (rx_buffer[readpos-3] == checkchar){ /*Compares the verfication char with the checkchar reesult*/
			switch (checkchar){	/*list of different cases*/
				case '0':
				puts("\ncheck is valid on code 0\nON during 10s");
				LED_TOGGLE;_delay_ms(10000);LED_TOGGLE; break;

				case '1':
				puts("\ncheck is valid on code 1\nON during 1s");
				LED_TOGGLE;_delay_ms(1000);LED_TOGGLE; break;

				case '2':
				puts("\ncheck is valid on code 1\nON during 2s");
				LED_TOGGLE;_delay_ms(2000);LED_TOGGLE; break;

				case '3':
				puts("\ncheck is valid on code 1\nON during 3s");
				LED_TOGGLE;_delay_ms(3000);LED_TOGGLE; break;

				case '4':
				puts("\ncheck is valid on code 1\nON during 4s");
				LED_TOGGLE;_delay_ms(4000);LED_TOGGLE; break;

				case '5':
				puts("\ncheck is valid on code 1\nON during 5s");
				LED_TOGGLE;_delay_ms(5000);LED_TOGGLE; break;

				case '6':
				puts("\ncheck is valid on code 1\nON during 6s");
				LED_TOGGLE;_delay_ms(6000);LED_TOGGLE; break;

				case '7':
				puts("\ncheck is valid on code 1\nON during 7s");
				LED_TOGGLE;_delay_ms(7000);LED_TOGGLE; break;

				case '8':
				puts("\ncheck is valid on code 1\nON during 8s");
				LED_TOGGLE;_delay_ms(8000);LED_TOGGLE; break;

				case '9':
				puts("\ncheck is valid on code 1\nON during 9s");
				LED_TOGGLE;_delay_ms(9000);LED_TOGGLE; break;

				default : break;
			}
		}
		else{
			puts("check is not valid\n");
		}

		memset(rx_buffer, 0, sizeof(rx_buffer));

		//LED_TOGGLE;

		commandflag=0;
		checksum=0;
		readpos=0;
		readflag=0;
	}
}
	

ISR(USART_RX_vect){	//Interruptor service routine. When char is pressed this interruptor is lauched

	uint8_t c = UDR0;
	loop_until_bit_is_set(UCSR0A, UDRE0);	// wait until we can send a new byte 
	UDR0 = (uint8_t) c;
	
	if (readflag==1){		//building the array of chars
		read_serial(c);
	}

	if (c=='!'){			//when ! is pressed readflag=1 and the next char are going to the rx_buffer
		readflag=1;
	}

	if (c=='\r'){			//When enter is pressed  commandflag for protocol is 1t
		for (size_t i = 0; i < readpos-2; i++)
		{
			checksum+=rx_buffer[i];	//the ASCII values of the rx_buff before \r and verification integer are summed
		}
		commandflag=1;
	}
}


int main(void) {

	/* tell stdio to use our stream as default */
	stdout=&mystdout;
	//stdin=&mystdin;

	/* pin config */
	DDRB = (1 << DDB5);

	/* uart config */
	UCSR0A = (1 << U2X0); /* this sets U2X0 to 1 */
	UCSR0B = (1 << RXEN0) | (1 << TXEN0) | (1 << RXCIE0);
	UCSR0C = (3 << UCSZ00); /* this sets UCSZ00 to 3 */

	/* baudrate setings (variable set by macros) */
	UBRR0H = (UBRRVAL) >> 8;
	UBRR0L = UBRRVAL;

	/* ADC cfg */
	sei();							  /* enable interrupts */


	// alinea 2
	for(;;){
		//puts("Grupo02\n\r");
		protocol_receiver();
		//_delay_ms(1000);
	}
}
